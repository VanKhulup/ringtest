package vankhulup.ringtest.dagger;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by vankhulup on 1/16/18
 */
@Qualifier
@Retention(RetentionPolicy.SOURCE)
public @interface RedditRetrofit {
}
