package vankhulup.ringtest.dagger;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import vankhulup.ringtest.MainApplication;
import vankhulup.ringtest.modules.AppModule;
import vankhulup.ringtest.modules.BindingModule;
import vankhulup.ringtest.modules.NetworkModule;
import vankhulup.ringtest.modules.StorageModule;

/**
 * Created by vankhulup on 1/15/18
 */

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class,
        StorageModule.class, AndroidInjectionModule.class,
        BindingModule.class})
public interface AppComponent extends AndroidInjector<MainApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        AppComponent.Builder application(Application application);

        AppComponent build();
    }
}
