package vankhulup.ringtest;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import vankhulup.ringtest.model.ItemModel;
import vankhulup.ringtest.util.OnCoverClickListener;
import vankhulup.ringtest.util.Utils;

/**
 * Created by vankhulup on 1/16/18
 */

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private List<ItemModel> mItemModels = new ArrayList<>();
    private OnCoverClickListener mOnCoverClickListener;
    private final Context mContext;

    public ListAdapter(Context context, OnCoverClickListener coverClickListener) {
        this.mOnCoverClickListener = coverClickListener;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_thing_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final ItemModel itemModel = mItemModels.get(position);
        holder.mTitle.setText(itemModel.getTitle());
        holder.mAuthor.setText(itemModel.getAuthor());
        holder.mCommentCount.setText(String.valueOf(itemModel.getCommentCount()));
        holder.mCover.setTransitionName("transitionName" + position);
        holder.mTimeStamp.setText(Utils.formatTimestamp(TimeUnit.SECONDS.toMillis(itemModel.getPublishTimestamp())));

        if (!TextUtils.isEmpty(itemModel.getCoverUrl())) {
            Picasso.with(mContext)
                    .load(itemModel.getCoverUrl())
                    .into(holder.mCover);
        }

        holder.mCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mOnCoverClickListener != null) {
                    mOnCoverClickListener.onCoverClicked(holder.mCover, itemModel.getCoverUrl());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItemModels.size();
    }

    public void addItems(List<ItemModel> items) {
        int prevCount = mItemModels.size();
        mItemModels.addAll(items);
        notifyItemRangeInserted(prevCount, items.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView mCover;
        private TextView mTitle;
        private TextView mAuthor;
        private TextView mCommentCount;
        private TextView mTimeStamp;

        public ViewHolder(View itemView) {
            super(itemView);
            mCover = itemView.findViewById(R.id.cover);
            mTitle = itemView.findViewById(R.id.title);
            mAuthor = itemView.findViewById(R.id.author);
            mCommentCount = itemView.findViewById(R.id.comment_count);
            mTimeStamp = itemView.findViewById(R.id.timestamp);
        }
    }
}
