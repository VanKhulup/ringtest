package vankhulup.ringtest;

import java.util.List;

import vankhulup.ringtest.model.ItemModel;

/**
 * Created by vankhulup on 1/15/18
 */

public interface IContract {

    interface Presenter {
        void takeView(View view);
        void onStart();
        void handleAuth(String url);
        void loadNext(boolean clearCache);
        String getCursor();
        void restoreCursor(String cursor);
    }

    interface View {
        void sendAuth(String url);
        void onItemsLoaded(List<ItemModel> items);
        void showProgress();
        void hideProgress();
    }
}
