package vankhulup.ringtest;

import java.util.List;

import javax.inject.Inject;

import vankhulup.ringtest.model.AuthModel;
import vankhulup.ringtest.model.ItemModel;

/**
 * Created by vankhulup on 1/15/18
 */

public class MainPresenter implements IContract.Presenter {

    public static final int PAGE_SIZE = 5;
    private IContract.View mView;
    private MainRepository mMainRepository;
    private String mNext;

    @Inject
    public MainPresenter(MainRepository repository) {
        this.mMainRepository = repository;
    }

    @Override
    public void onStart() {
        if (!mMainRepository.hasValidToken()) {
            mView.sendAuth(mMainRepository.buildAuthUrl());
        } else {
            loadNext(true);
        }
    }

    @Override
    public void takeView(IContract.View view) {
        this.mView = view;
    }

    @Override
    public void handleAuth(String accessCode) {
        mMainRepository.getAccessToken(accessCode, new RepositoryCallback<AuthModel>() {
            @Override
            public void onSuccess(AuthModel result) {
                mMainRepository.clearCache();
                loadNext(false);
            }

            @Override
            public void onError() {

            }
        });
    }

    public void loadNext(boolean canUseCache) {
        mView.showProgress();

        mMainRepository.getRedditTop(canUseCache, new RepositoryCallback<List<ItemModel>>() {
            @Override
            public void onSuccess(List<ItemModel> result) {
                mView.hideProgress();
                mView.onItemsLoaded(result);
            }

            @Override
            public void onError() {
                mView.hideProgress();
            }
        });
    }

    @Override
    public String getCursor() {
        return mMainRepository.getNext();
    }

    @Override
    public void restoreCursor(String cursor) {
        mMainRepository.setNext(cursor);
    }
}
