package vankhulup.ringtest;

/**
 * Created by vankhulup on 1/15/18
 */

public interface RepositoryCallback<T> {

    void onSuccess(T result);
    void onError();
}
