package vankhulup.ringtest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vankhulup on 1/15/18
 */

public class AuthModel {
    @SerializedName("access_token")
    private String mAccessToken;
    @SerializedName("token_type")
    private String mTokenType;
    @SerializedName("refresh_token")
    private String mRefreshToken;
    @SerializedName("expires_in")
    private int mExpirationPeriod;
    @SerializedName("scope")
    private String mScope;


    public AuthModel() {
    }

    public AuthModel(String accessToken) {
        mAccessToken = accessToken;
    }

    public String getAccessToken() {
        return mAccessToken;
    }

    public String getTokenType() {
        return mTokenType;
    }

    public String getRefreshToken() {
        return mRefreshToken;
    }

    public int getExpirationPeriod() {
        return mExpirationPeriod;
    }

    public String getScope() {
        return mScope;
    }
}
