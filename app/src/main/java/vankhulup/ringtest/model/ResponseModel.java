package vankhulup.ringtest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vankhulup on 1/16/18
 */

public class ResponseModel {
    @SerializedName("data")
    PageModel mPageModel;

    public PageModel getPageModel() {
        return mPageModel;
    }
}
