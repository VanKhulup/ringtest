package vankhulup.ringtest.model;

/**
 * Created by vankhulup on 1/16/18
 */

public class MediaModel {

    private String mUrl;

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String url) {
        mUrl = url;
    }
}
