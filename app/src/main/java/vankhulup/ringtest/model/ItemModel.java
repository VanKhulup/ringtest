package vankhulup.ringtest.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vankhulup on 1/16/18
 */

public class ItemModel {
    @SerializedName("data")
    private Data mData;

    public String getTitle() {
        return mData.title;
    }

    public String getAuthor() {
        return mData.author;
    }

    public int getCommentCount() {
        return mData.commentsCount;
    }

    public String getCoverUrl() {
        return mData.mediaModel != null ? mData.mediaModel.getUrl() : "";
    }

    public long getPublishTimestamp() {
        return mData.created;
    }

    public static class Data {
        @SerializedName("title")
        String title;
        @SerializedName("author")
        String author;
        @SerializedName("created_utc")
        long created;
        @SerializedName("num_comments")
        int commentsCount;
        @SerializedName("preview")
        MediaModel mediaModel;
    }
}
