package vankhulup.ringtest.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vankhulup on 1/16/18
 */

public class PageModel {
    @SerializedName("children")
    private List<ItemModel> mData;

    @SerializedName("after")
    private String mNextCursor;

    public List<ItemModel> getData() {
        return mData;
    }

    public void setData(List<ItemModel> data) {
        mData = data;
    }

    public String getNextCursor() {
        return mNextCursor;
    }

    public void setNextCursor(String nextCursor) {
        mNextCursor = nextCursor;
    }
}
