package vankhulup.ringtest;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import vankhulup.ringtest.dagger.DaggerAppComponent;

/**
 * Created by vankhulup on 1/15/18
 */

public class MainApplication extends DaggerApplication {

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerAppComponent.builder().application(this).build();
    }
}
