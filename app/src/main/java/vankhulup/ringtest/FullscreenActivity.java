package vankhulup.ringtest;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.widget.ImageView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by vankhulup on 1/16/18
 */

public class FullscreenActivity extends Activity {
    public static final String EXTRA_URL = "url";
    public static final String EXTRA_TRANSITION_NAME = "transitionName";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);
        postponeEnterTransition();
        if (getIntent().getExtras() != null) {
            Bundle payload = getIntent().getExtras();
            String url = payload.getString(EXTRA_URL);
            String transitionName = payload.getString(EXTRA_TRANSITION_NAME);
            ImageView imageView = findViewById(R.id.fullscreen_image);
            imageView.setTransitionName(transitionName);
            if (!TextUtils.isEmpty(url)) {
                Picasso.with(this)
                        .load(url)
                        .into(imageView, new Callback() {
                            @Override
                            public void onSuccess() {
                                startPostponedEnterTransition();
                            }

                            @Override
                            public void onError() {
                                startPostponedEnterTransition();
                            }
                        });

            } else {
                finish();
            }
        }

    }
}
