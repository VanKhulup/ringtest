package vankhulup.ringtest;

import android.content.SharedPreferences;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vankhulup.ringtest.model.AuthModel;
import vankhulup.ringtest.model.ItemModel;
import vankhulup.ringtest.model.ResponseModel;
import vankhulup.ringtest.modules.AuthApi;
import vankhulup.ringtest.modules.NetworkModule;
import vankhulup.ringtest.modules.RedditApi;
import vankhulup.ringtest.modules.StorageModule;


/**
 * Created by vankhulup on 1/15/18
 */
@Singleton
public class MainRepository {

    private final AuthApi mAuthApi;
    private final RedditApi mRedditApi;
    private final SharedPreferences mStorageOptions;
    private AuthModel mAuthModel;

    private List<ItemModel> mInMemoryCache = new ArrayList<>();

    private String mNext;

    @Inject
    public MainRepository(AuthApi authApi, RedditApi redditApi, SharedPreferences storageOption) {
        this.mAuthApi = authApi;
        this.mRedditApi = redditApi;
        this.mStorageOptions = storageOption;
    }

    public String buildAuthUrl() {
        return String.format(NetworkModule.AUTH_URL_PATTERN, NetworkModule.CLIENT_ID, NetworkModule.RESPONSE_TYPE, NetworkModule.STATE, NetworkModule.REDIRECT_URL, NetworkModule.DURATION, NetworkModule.SCOPE);
    }

    public void getAccessToken(String accessCode, @Nullable final RepositoryCallback<AuthModel> callback) {
        String basicAuth = android.util.Base64.encodeToString((NetworkModule.CLIENT_ID + ":").getBytes(), android.util.Base64.NO_WRAP);
        Call<AuthModel> call = mAuthApi.authorize("Basic " + basicAuth, "authorization_code", accessCode, NetworkModule.REDIRECT_URL);
        call.enqueue(new Callback<AuthModel>() {
            @Override
            public void onResponse(Call<AuthModel> call, Response<AuthModel> response) {
                if (response.isSuccessful()) {
                    mAuthModel = response.body();
                    saveToStorage();
                    if (callback != null) {
                        callback.onSuccess(mAuthModel);
                    }
                }
            }

            @Override
            public void onFailure(Call<AuthModel> call, Throwable t) {
                if (callback != null) {
                    callback.onError();
                }
            }
        });
    }

    private void saveToStorage() {
        Calendar calendar = Calendar.getInstance();
        mStorageOptions.edit()
                .putString(StorageModule.KEY_ACCESS_TOKEN, mAuthModel.getAccessToken())
                .putString(StorageModule.KEY_REFRESH_TOKEN, mAuthModel.getRefreshToken())
                .putLong(StorageModule.KEY_OBTAIN_TIMESTAMP, calendar.getTimeInMillis())
                .apply();
    }

    public void getRedditTop(boolean cacheSuitable, final RepositoryCallback<List<ItemModel>> callback) {

        if (cacheSuitable && !mInMemoryCache.isEmpty()) {
            if (callback != null) {
                callback.onSuccess(mInMemoryCache);
                return;
            }
        }

        String authHeader = "Bearer " + mAuthModel.getAccessToken();
        Call<ResponseModel> call = mRedditApi.getTop(authHeader, MainPresenter.PAGE_SIZE, mNext);

        if (mNext == null) mInMemoryCache.clear();

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (callback != null) {
                    ResponseModel responseModel = response.body();
                    if (responseModel != null) {
                        mInMemoryCache.addAll(responseModel.getPageModel().getData());
                        mNext = responseModel.getPageModel().getNextCursor();
                        callback.onSuccess(responseModel.getPageModel().getData());
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
            }
        });
    }

    public boolean hasValidToken() {
        String accessToken = mStorageOptions.getString(StorageModule.KEY_ACCESS_TOKEN, null);
        long tokenTimestamp = mStorageOptions.getLong(StorageModule.KEY_OBTAIN_TIMESTAMP, 0);
        long tokenEndTime = tokenTimestamp + TimeUnit.SECONDS.toMillis(3600);

        Calendar calendar = Calendar.getInstance();
        if (mAuthModel != null) {
            return true;
        } else if (!TextUtils.isEmpty(accessToken) && (new Date(tokenEndTime).after(calendar.getTime()))) {
            mAuthModel = new AuthModel(accessToken);
            return true;
        }
        return false;
    }

    public String getNext() {
        return mNext;
    }

    public void setNext(String next) {
        mNext = next;
    }

    public void clearCache() {
        mInMemoryCache.clear();
    }
}
