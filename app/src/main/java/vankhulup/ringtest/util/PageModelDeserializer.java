package vankhulup.ringtest.util;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import vankhulup.ringtest.model.PageModel;

/**
 * Created by vankhulup on 1/16/18
 */

public class PageModelDeserializer implements JsonDeserializer<PageModel>{
    @Override
    public PageModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        PageModel pageModel = new PageModel();

        return pageModel;
    }
}
