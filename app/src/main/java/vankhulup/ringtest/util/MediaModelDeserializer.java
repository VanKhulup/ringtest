package vankhulup.ringtest.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

import vankhulup.ringtest.model.MediaModel;

/**
 * Created by vankhulup on 1/16/18
 */

public class MediaModelDeserializer implements JsonDeserializer<MediaModel> {
    @Override
    public MediaModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        MediaModel mediaModel = new MediaModel();
        JsonArray imagesArray = json.getAsJsonObject().get("images").getAsJsonArray();
        if (imagesArray.size() > 0) {
            JsonObject imageObject = imagesArray.get(0).getAsJsonObject();
            JsonObject sourceDataObje = imageObject.get("source").getAsJsonObject();
            if (sourceDataObje != null) {
                String sourceUrl = sourceDataObje.get("url").getAsString();
                mediaModel.setUrl(sourceUrl);
                return mediaModel;
            }
        }

        return null;
    }
}
