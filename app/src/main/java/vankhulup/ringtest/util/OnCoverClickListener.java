package vankhulup.ringtest.util;

import android.widget.ImageView;

/**
 * Created by vankhulup on 1/16/18
 */

public interface OnCoverClickListener {
    void onCoverClicked(ImageView mClickedView, String url);
}
