package vankhulup.ringtest.util;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by vankhulup on 1/16/18
 */

public class BaseEndlessRecyclerScrollListener extends RecyclerView.OnScrollListener {
    private final LinearLayoutManager mLinearLayoutManager;
    protected boolean mIsLoadingLocked;

    protected static final int VISIBLE_THRESHOLD = 3; // The minimum amount of items to have below your current scroll position before mIsLoading more.

    private int mPreviousTotal = 0;
    private boolean mIsLoading = true;
    private OnLoadMoreListener mMoreListener;

    public BaseEndlessRecyclerScrollListener(LinearLayoutManager linearLayoutManager) {
        mLinearLayoutManager = linearLayoutManager;
    }

    public void setMoreListener(OnLoadMoreListener moreListener) {
        mMoreListener = moreListener;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int firstVisibleItem = mLinearLayoutManager.findFirstVisibleItemPosition();
        int visibleItemCount = recyclerView.getChildCount();
        int totalItemCount = mLinearLayoutManager.getItemCount();

        onScroll(firstVisibleItem, visibleItemCount, totalItemCount);
    }

    public void onScroll(int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if (mIsLoadingLocked) return;

        if (mIsLoading) {
            if (totalItemCount > mPreviousTotal) {
                mIsLoading = false;
                mPreviousTotal = totalItemCount;
            }
        } else if ((totalItemCount - visibleItemCount) <= (firstVisibleItem + VISIBLE_THRESHOLD)) {
            if (mMoreListener != null) {
                mIsLoading = true;
                mMoreListener.onLoadNextPage();
            }

        }
    }

    public interface OnLoadMoreListener {
        void onLoadNextPage();
    }
}
