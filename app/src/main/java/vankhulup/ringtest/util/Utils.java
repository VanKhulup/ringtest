package vankhulup.ringtest.util;

import android.text.format.DateUtils;
import android.text.format.Time;

import java.text.DateFormatSymbols;
import java.util.Locale;

/**
 * Created by vankhulup on 1/16/18
 */

public class Utils {
    private static final String[] sShortMonths = new DateFormatSymbols().getShortMonths();
    private static final Time sTime = new Time();

    public static String formatTimestamp(long timestamp) {
        synchronized (sTime) {
            sTime.setToNow();
            final long currentTime = sTime.toMillis(false);

            sTime.set(timestamp);
            final long targetTime = sTime.toMillis(false);
            final int targetMonth = sTime.month;

            long timeDiff = currentTime - targetTime;
            long elapsedMinutes = timeDiff / DateUtils.MINUTE_IN_MILLIS;
            long elapsedHours = timeDiff / DateUtils.HOUR_IN_MILLIS;
            long elapsedDays = timeDiff / DateUtils.DAY_IN_MILLIS;
            long elapsedWeeks = timeDiff / DateUtils.WEEK_IN_MILLIS;
            long elapsedMonths = (long) ((float) elapsedDays / ((DateUtils.YEAR_IN_MILLIS / DateUtils.DAY_IN_MILLIS) / 12));

            String month = sShortMonths[targetMonth];
            // Year
            if (elapsedMonths > 12) {
                return String.format(Locale.getDefault(), "%s %te, %tY", month, timestamp, timestamp);
            } else if (elapsedMonths >= 2) {
                return String.format(Locale.getDefault(), "%s %te", month, timestamp);
            } else if (elapsedDays > 30) {
                return "1 mo";
            }
            // Month in weeks
            if (elapsedDays >= 7) {
                return String.format("%dw", elapsedWeeks);
            }
            // Week in days
            if (elapsedDays >= 1) {
                return String.format("%dd", elapsedDays);
            }
            // Day in hours
            if (elapsedHours >= 1) {
                return String.format("%dh", elapsedHours);
            }
            // Hour in minutes
            if (elapsedMinutes >= 1) {
                return String.format("%dm", elapsedMinutes);
            } else {
                return "just now";
            }
        }
    }
}
