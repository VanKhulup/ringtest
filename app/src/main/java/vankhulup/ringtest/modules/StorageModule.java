package vankhulup.ringtest.modules;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by vankhulup on 1/16/18
 */
@Module
public class StorageModule {
    public static final String PREFS_NAME = "RingTest";
    public static final String KEY_ACCESS_TOKEN = "access_token";
    public static final String KEY_REFRESH_TOKEN = "refresh_token";
    public static final String KEY_OBTAIN_TIMESTAMP = "obtain_timestamp";

    public StorageModule() {
    }

    @Provides
    @Singleton
    SharedPreferences providePreferences(Context context) {
        return context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }
}
