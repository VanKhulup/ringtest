package vankhulup.ringtest.modules;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;
import vankhulup.ringtest.model.ResponseModel;

/**
 * Created by vankhulup on 1/16/18
 */

public interface RedditApi {

    @GET("top")
    Call<ResponseModel> getTop(@Header("Authorization") String authHeader, @Query("limit") int limit, @Query("after") String cursor);
}
