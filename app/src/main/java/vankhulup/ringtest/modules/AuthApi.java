package vankhulup.ringtest.modules;


import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import vankhulup.ringtest.model.AuthModel;

/**
 * Created by vankhulup on 1/14/18
 */

public interface AuthApi {
    @FormUrlEncoded
    @POST("access_token")
    Call<AuthModel> authorize(@Header("Authorization")String basicAuth, @Field(value = "grant_type") String grantType,
                              @Field(value = "code")String code, @Field("redirect_uri")String redirectUrl);
}
