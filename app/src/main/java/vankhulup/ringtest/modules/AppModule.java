package vankhulup.ringtest.modules;

import android.app.Application;
import android.content.Context;

import dagger.Binds;
import dagger.Module;

/**
 * Created by vankhulup on 1/16/18
 */
@Module
public abstract class AppModule {
    @Binds
    abstract Context bindContext(Application application);
}
