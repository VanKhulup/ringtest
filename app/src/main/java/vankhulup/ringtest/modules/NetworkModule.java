package vankhulup.ringtest.modules;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import vankhulup.ringtest.dagger.AuthRetrofit;
import vankhulup.ringtest.dagger.RedditRetrofit;
import vankhulup.ringtest.model.MediaModel;
import vankhulup.ringtest.util.MediaModelDeserializer;

/**
 * Created by vankhulup on 1/14/18
 */
@Module
public class NetworkModule {
    public static final String BASE_URL = "https://www.reddit.com/api/v1/";
    public static final String BASE_OAUTH_URL = "https://www.oauth.reddit.com/";

    public static final String AUTH_URL_PATTERN = "https://www.reddit.com/api/v1/authorize.compact?client_id=%s&response_type=%s&state=%s&redirect_uri=%s&duration=%s&scope=%s";

    public static final String CLIENT_ID = "WJD1-ZxqZItPOA";
    public static final String RESPONSE_TYPE = "code";
    public static final String REDIRECT_URL = "https://bitbucket.org/VanKhulup/ringtest";
    public static final String DURATION = "permanent";
    public static final String SCOPE = "read";
    public static final String STATE = "asdadaq";

    public NetworkModule() {
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(MediaModel.class, new MediaModelDeserializer());
        return builder.create();
    }

    /*I don't know what is wrong with custom SSL certificates, but for some reason OkHttpClient throws
    * javax.net.ssl.SSLPeerUnverifiedException: Hostname oauth.reddit.com not verified.  */
    @Provides
    @Singleton
    OkHttpClient provideHttpClient() {
        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
            OkHttpClient okHttpClient = builder.build();
            return okHttpClient;

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Provides
    @Singleton
    @AuthRetrofit
    Retrofit provideAuthRetrofit(OkHttpClient client, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    @RedditRetrofit
    Retrofit provideRegularRetrofit(OkHttpClient client, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(BASE_OAUTH_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
    }

    @Provides
    @Singleton
    AuthApi provideAuthAPI(@AuthRetrofit Retrofit retrofit) {
        return retrofit.create(AuthApi.class);
    }

    @Provides
    @Singleton
    RedditApi provideRegularAPI(@RedditRetrofit Retrofit retrofit) {
        return retrofit.create(RedditApi.class);
    }

}
