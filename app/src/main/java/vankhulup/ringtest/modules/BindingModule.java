package vankhulup.ringtest.modules;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import vankhulup.ringtest.MainActivity;

/**
 * Created by vankhulup on 1/15/18
 */
@Module
public abstract class BindingModule {
    @ContributesAndroidInjector
    abstract MainActivity inject();
}
