package vankhulup.ringtest;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import vankhulup.ringtest.model.ItemModel;
import vankhulup.ringtest.modules.NetworkModule;
import vankhulup.ringtest.util.BaseEndlessRecyclerScrollListener;
import vankhulup.ringtest.util.OnCoverClickListener;

public class MainActivity extends Activity implements IContract.View, OnCoverClickListener, BaseEndlessRecyclerScrollListener.OnLoadMoreListener {

    private RecyclerView mMainList;
    private ProgressBar mLoadingProgress;
    private ListAdapter mAdapter;

    @Inject
    MainPresenter mMainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMainList = findViewById(R.id.main_list);
        mLoadingProgress = findViewById(R.id.progress);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mMainList.setLayoutManager(layoutManager);
        mAdapter = new ListAdapter(this, this);
        mMainList.setAdapter(mAdapter);
        mMainList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        BaseEndlessRecyclerScrollListener scrollListener = new BaseEndlessRecyclerScrollListener(layoutManager);
        scrollListener.setMoreListener(this);
        mMainList.clearOnScrollListeners();
        mMainList.addOnScrollListener(scrollListener);

        AndroidInjection.inject(this);
        mMainPresenter.takeView(this);

        Uri uri = getIntent().getData();
        if (Intent.ACTION_VIEW.equals(getIntent().getAction()) && uri != null) {
            String state = uri.getQueryParameter("state");
            if (state.equals(NetworkModule.STATE)) {
                String code = uri.getQueryParameter("code");
                mMainPresenter.handleAuth(code);
                getIntent().setData(null);
            }
        } else {
            mMainPresenter.onStart();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String cursor = mMainPresenter.getCursor();
        outState.putString("cursor", cursor);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            mMainPresenter.restoreCursor(savedInstanceState.getString("cursor"));
        }
    }

    private void startTransition(ImageView source, String url) {
        Intent intent = new Intent(this, FullscreenActivity.class);
        intent.putExtra(FullscreenActivity.EXTRA_URL, url);
        intent.putExtra(FullscreenActivity.EXTRA_TRANSITION_NAME, source.getTransitionName());

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this,
                source,
                source.getTransitionName());

        startActivity(intent, options.toBundle());
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void sendAuth(String url) {
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void onItemsLoaded(List<ItemModel> items) {
        mAdapter.addItems(items);
    }

    @Override
    public void hideProgress() {
        mLoadingProgress.setVisibility(View.GONE);
    }

    @Override
    public void showProgress() {
        mLoadingProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCoverClicked(ImageView mClickedView, String url) {
        startTransition(mClickedView, url);
    }

    @Override
    public void onLoadNextPage() {
        mMainPresenter.loadNext(false);
    }
}
